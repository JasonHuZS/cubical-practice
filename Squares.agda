{-# OPTIONS --cubical --safe #-}

-- properties about general squares
module Squares where

open import Cubical.Foundations.Prelude

variable
  ℓ : Level
  A : Set ℓ
  a b c d : A

-- prove the following square
--      q
-- b ------ c
-- |        |
-- |p       |p
-- |    q   |
-- a ------ b
--
-- this is proved by an hcomp:
--
--          q
--      b ---- c
--    p/|     /|
--    / |   q/ |
--   /  |p  /  |q
--  a ---- b   |
--  |   |  |   |
--  |   b -|-- b
--  | p/   |  /
--  | /    | /
--  |/  p  |/
--  a ---- b
--
-- where the target square is given by the lid of the cube
square₀ : (p : a ≡ b) (q : b ≡ c) → PathP (λ i → p i ≡ q i) p q
square₀ p q i j = hcomp (λ k → λ { (i = i0) → p j
                                 ; (i = i1) → q (j ∧ k)
                                 ; (j = i0) → p i
                                 ; (j = i1) → q (i ∧ k) }) (p (i ∨ j))

module _ (p q : a ≡ b) (pq : PathP (λ i → a ≡ b) p q) where

  --      q
  -- a ------ b
  -- |        |
  -- |l       |r
  -- |    p   |
  -- a ------ b

    private
      l : a ≡ a
      l i = pq i i0

      r : b ≡ b
      r i = pq i i1

    lr-eq : PathP (λ i → p i ≡ q i) l r
    lr-eq i j = pq j i
